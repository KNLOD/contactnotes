package com.example.contactnotes

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.model.note.entities.Note
import com.google.android.material.floatingactionbutton.FloatingActionButton

class NotesAdapter(private val dataset : List<Note>,
                   private val viewModel : ContactViewModel
                   ) : RecyclerView.Adapter<NotesAdapter.NotesViewHolder>(){
    class NotesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val deleteNote : FloatingActionButton = itemView.findViewById(R.id.delete_note)
        val openNote : FloatingActionButton = itemView.findViewById(R.id.open_note)
        val titleNote : TextView = itemView.findViewById(R.id.note_title)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.note_list_item, parent, false)
        return NotesViewHolder(adapterLayout)

    }

    override fun onBindViewHolder(holder : NotesViewHolder, position : Int){
        val currentNote : Note = dataset[position]
        holder.titleNote.text = currentNote.noteTitle
        holder.openNote.setOnClickListener{
            viewModel.setChangingMode(ContactViewModel.Mode.UPDATE)
            viewModel.setCurrentNote(currentNote)
            Navigation.findNavController(it).navigate(R.id.noteFragment)
        }
        holder.deleteNote.setOnClickListener{
            Log.e("DELETE", "$currentNote")
            viewModel.deleteNote(currentNote)
        }



    }

    override fun getItemCount() : Int{
        return dataset.size
    }
}