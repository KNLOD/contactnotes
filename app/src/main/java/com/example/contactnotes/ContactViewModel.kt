package com.example.contactnotes

import android.util.Log
import androidx.lifecycle.*
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.contact.room.RoomContactsRepository
import com.example.contactnotes.model.note.entities.Note
import com.example.contactnotes.model.note.room.RoomNotesRepository
import kotlinx.coroutines.launch

class ContactViewModel(
    private val contactsRepository : RoomContactsRepository,
    private val notesRepository : RoomNotesRepository
) : ViewModel() {
    enum class Mode{
        UPDATE, ADD
    }
    /**
     * Factory class for creating viewModel wiht repositories
     */
    class ContactViewModelFactory(private val contactsRepository : RoomContactsRepository,
                                  private val notesRepository: RoomNotesRepository) : ViewModelProvider.Factory{
        override fun <T : ViewModel> create(modelClass : Class<T>) : T{
            return modelClass.getConstructor(RoomContactsRepository::class.java, RoomNotesRepository::class.java)
                .newInstance(contactsRepository, notesRepository) as T
        }


    }
    private var _currentContact = MutableLiveData<Contact>()
    val currentContact : LiveData<Contact>
    get() = _currentContact

    private var _contactsDataset = MutableLiveData<List<Contact>>()
    val contactsDataset : LiveData<List<Contact>>
    get() = _contactsDataset

    private var _currentNote = MutableLiveData<Note>()
    val currentNote : LiveData<Note>
    get() = _currentNote

    private var _notesDataset = MutableLiveData<List<Note>>()
    val notesDataset : LiveData<List<Note>>
    get() = _notesDataset

    private var _changingMode = MutableLiveData<Mode>(Mode.ADD)
    val changingMode : LiveData<Mode>
    get() = _changingMode


    fun setChangingMode(mode : Mode){
        _changingMode.value = mode
    }

    fun updateDataset(){
        viewModelScope.launch {
            val contacts = contactsRepository.getAllContacts()
            _contactsDataset.value = contacts
        }

    }


    fun setCurrentContact(contact : Contact){
        _currentContact.value = contact
    }
    fun setNotesDatasetByContact(contact: Contact){
        viewModelScope.launch {
            val dataset = notesRepository.getContactNotes(contact)
            _notesDataset.value = dataset
        }

    }
    fun setCurrentNote(note : Note){
        _currentNote.value = note
    }
    fun updateNote(note : Note){
        viewModelScope.launch{
            notesRepository.updateNote(
                NoteData(
                    noteId = note.noteId,
                    noteTitle = note.noteTitle,
                    noteText = note.noteText,
                    contactId = currentContact.value!!.id
                )
            )
            setNotesDatasetByContact(currentContact.value!!)
        }
    }
    fun deleteNote(note : Note){
        viewModelScope.launch {
            notesRepository.deleteNote(
                NoteData(
                    noteId = note.noteId,
                    noteText = note.noteText,
                    noteTitle = note.noteTitle,
                    contactId = currentContact.value!!.id

                )
            )
            setNotesDatasetByContact(currentContact.value!!)
        }

    }
    fun addNote(note : Note){
        viewModelScope.launch{
            notesRepository.createNote(NoteData(
                noteId = 0,
                noteTitle = note.noteTitle,
                noteText = note.noteText,
                contactId = currentContact.value!!.id
            ))
        }
    }

    init{
        viewModelScope.launch{
            val contacts = contactsRepository.getAllContacts()
            _contactsDataset.value = contacts
            Log.e("ViewModel", "$contacts")
        }
    }


}