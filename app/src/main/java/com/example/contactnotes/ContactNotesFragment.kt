package com.example.contactnotes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.model.note.entities.Note
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * A simple [Fragment] subclass.
 * Use the [ContactNotesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactNotesFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_notes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView : RecyclerView = view.findViewById(R.id.recycler_view)
        val addButton : FloatingActionButton = view.findViewById(R.id.add_button)

        val viewModel : ContactViewModel by activityViewModels()
        viewModel.currentContact.observe(viewLifecycleOwner, Observer{
            viewModel.setNotesDatasetByContact(it)
        })
        viewModel.notesDataset.observe(viewLifecycleOwner, Observer{
            recyclerView.adapter = NotesAdapter(it, viewModel)
        })

        addButton.setOnClickListener{
            viewModel.setCurrentNote(Note())
            viewModel.setChangingMode(ContactViewModel.Mode.ADD)
            Navigation.findNavController(view).navigate(R.id.noteFragment)
        }


    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactNotesFragment()
    }
}