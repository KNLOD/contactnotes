package com.example.contactnotes

import android.Manifest
import android.content.ContentResolver
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.NavHostFragment
import com.example.contactnotes.model.contact.ContactsLocalDataSource
import com.example.contactnotes.model.contact.entities.Contact
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    companion object{
        private const val CONTACTS_REQUEST_CODE = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), CONTACTS_REQUEST_CODE)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        Repositories.init(applicationContext)
        val contacts = ContactsLocalDataSource(application.contentResolver).fetchContacts()

        GlobalScope.launch {

            Repositories.contactsRepository.updateContacts()


            val contactsRepository = Repositories.contactsRepository
            val notesRepository = Repositories.notesRepository

            val contactViewModel: ContactViewModel by viewModels {
                ContactViewModel.ContactViewModelFactory(contactsRepository, notesRepository)
            }
        }



}

    override fun onStart() {
        super.onStart()
        GlobalScope.launch {
            val contactsRepository = Repositories.contactsRepository
            val notesRepository = Repositories.notesRepository


            val contactViewModel: ContactViewModel by viewModels {
                ContactViewModel.ContactViewModelFactory(contactsRepository, notesRepository)
            }
            contactsRepository.updateContacts()
            contactViewModel.updateDataset()





        }
    }

    private fun requestPermissions(readContacts: String) {

    }

}
