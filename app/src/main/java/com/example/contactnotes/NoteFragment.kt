package com.example.contactnotes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.contactnotes.model.note.entities.Note
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText

/**
 * A simple [Fragment] subclass.
 * Use the [NoteFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NoteFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val noteTitleEdit : TextInputEditText = view.findViewById(R.id.title_edit)
        val noteEdit : TextInputEditText = view.findViewById(R.id.note_edit)
        val saveNote : FloatingActionButton = view.findViewById(R.id.save_button)

        val viewModel : ContactViewModel by activityViewModels{
            ContactViewModel.ContactViewModelFactory(Repositories.contactsRepository, Repositories.notesRepository)
        }

        viewModel.currentNote.observe(viewLifecycleOwner, Observer{
            noteTitleEdit.setText(it.noteTitle)
            noteEdit.setText(it.noteText)
        })
        saveNote.setOnClickListener{

            when (viewModel.changingMode.value) {
                ContactViewModel.Mode.ADD -> {

                    val note = Note(
                        noteId = 0,
                        noteTitle = noteTitleEdit.text.toString(),
                        noteText = noteEdit.text.toString()
                    )
                    viewModel.addNote(note)
                }
                ContactViewModel.Mode.UPDATE -> {
                    val note = Note(
                        noteId = viewModel.currentNote.value!!.noteId,
                        noteTitle = noteTitleEdit.text.toString(),
                        noteText = noteEdit.text.toString()
                    )

                    viewModel.updateNote(note)

                }

                else -> {}
            }


        }






    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = NoteFragment()
    }
}