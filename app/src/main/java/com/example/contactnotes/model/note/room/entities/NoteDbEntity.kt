package com.example.contactnotes.model.note.room.entities

import androidx.room.*
import com.example.contactnotes.NoteData
import com.example.contactnotes.model.contact.room.entities.ContactDbEntity
import com.example.contactnotes.model.note.entities.Note

@Entity(
    tableName = "notes",
    indices = [Index("note_id", unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = ContactDbEntity::class,
            parentColumns = ["id"],
            childColumns = ["contact_id"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class NoteDbEntity(
    @ColumnInfo(name = "contact_id") val contactId : Long,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "note_id") val noteId : Long,
    @ColumnInfo(name = "note_title") val noteTitle : String,
    @ColumnInfo(name = "note_text") val noteText : String,
){
    fun toNote() = Note(noteId, noteTitle, noteText)

    companion object{
        fun fromNoteData(noteData : NoteData) : NoteDbEntity = NoteDbEntity(
            noteId = noteData.noteId,
            contactId = noteData.contactId,
            noteTitle = noteData.noteTitle,
            noteText = noteData.noteText,
        )
    }

}
