package com.example.contactnotes.model.note.room

import com.example.contactnotes.NoteData
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.note.entities.Note
import com.example.contactnotes.model.note.room.entities.NoteDbEntity

class RoomNotesRepository(private val notesDao : NotesDao) {

    suspend fun getContactNotes(contact : Contact) : List<Note>{
        return notesDao.findNotesByContactId(contact.id).map{
            it.toNote()
        }
    }

    suspend fun createNote(noteData : NoteData){
        notesDao.createNote(NoteDbEntity.fromNoteData(noteData))
    }

    suspend fun deleteNote(noteData : NoteData){
        notesDao.deleteNote(noteData.noteId)
    }

    suspend fun updateNote(noteData : NoteData){
        notesDao.updateNote(NoteDbEntity.fromNoteData(noteData))
    }




}