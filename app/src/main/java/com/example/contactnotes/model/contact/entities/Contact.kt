package com.example.contactnotes.model.contact.entities

data class Contact(
    val id : Long,
    val contactName : String,
)
