package com.example.contactnotes.model.note.entities

data class Note(
    val noteId : Long = 0,
    val noteTitle : String = "",
    val noteText : String = "",

)
