package com.example.contactnotes.model.contact.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.contactnotes.model.contact.entities.Contact

@Entity(
    tableName = "contacts",
    indices = [
        Index("id", unique = true)
    ]
)
data class ContactDbEntity(
    @PrimaryKey val id : Long,
    @ColumnInfo(name = "contact_name") val contactName : String,
){
    fun toContact() = Contact(
        id = id,
        contactName = contactName
    )
    companion object{
       // TODO
    }
}
