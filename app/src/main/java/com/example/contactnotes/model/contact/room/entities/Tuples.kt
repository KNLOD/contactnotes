package com.example.contactnotes.model.contact.room.entities


data class ContactTuple(
    val id : Long,
    val contact_name: String,
)

