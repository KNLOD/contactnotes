package com.example.contactnotes.model.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.contactnotes.model.contact.room.ContactsDao
import com.example.contactnotes.model.contact.room.entities.ContactDbEntity
import com.example.contactnotes.model.note.room.NotesDao
import com.example.contactnotes.model.note.room.entities.NoteDbEntity

@Database(
    version = 1,
    entities = [
        ContactDbEntity::class,
        NoteDbEntity::class
    ]

)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getContactsDao() : ContactsDao

    abstract fun getNotesDao() : NotesDao



}