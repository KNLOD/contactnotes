package com.example.contactnotes.model.contact.room

import android.content.ContentResolver
import android.util.Log
import com.example.contactnotes.model.contact.ContactsLocalDataSource
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.contact.room.entities.ContactDbEntity

class RoomContactsRepository(private val contactsDao : ContactsDao,
                             private val contactsLocalDataSource : ContactsLocalDataSource
) {

    suspend fun getAllContacts() : List<Contact>{
        return contactsDao.getAllContacts().map{
            it.toContact()
        }
    }

    suspend fun addContact(contact : Contact){
        contactsDao.addContact(ContactDbEntity(
            id = contact.id,
            contactName = contact.contactName
        ))
    }

    suspend fun deleteContact(contact : Contact){
        contactsDao.deleteContact(
            ContactDbEntity(
            id = contact.id,
            contactName = contact.contactName
        )
        )
    }

    /**
     * Updates in DataBase contacts if contacts are added or deleted
     */
    suspend fun updateContacts(){
        val localContacts = fetchContacts(contactsLocalDataSource).toSet()
        val dbContacts = getAllContacts().toSet()

        val newLocalContacts = localContacts subtract dbContacts
        val deletedContacts = dbContacts subtract localContacts
        Log.e("UPDATE", "${newLocalContacts}, deleted: ${deletedContacts}")

        newLocalContacts.forEach{
            addContact(it)
        }
        deletedContacts.forEach{
            deleteContact(it)
        }
    }


    suspend fun loadContactsToDb(){
       fetchContacts(contactsLocalDataSource).map{
           addContact(it)
       }
    }

    fun fetchContacts(source: ContactsLocalDataSource) : List<Contact>{
        return source.fetchContacts()
    }


}