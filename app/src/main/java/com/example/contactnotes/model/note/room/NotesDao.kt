package com.example.contactnotes.model.note.room

import androidx.room.*
import com.example.contactnotes.model.note.room.entities.NoteDbEntity
import com.example.contactnotes.model.note.room.entities.NoteTuple

@Dao
interface NotesDao {

    @Insert
    suspend fun createNote(noteDbEntity : NoteDbEntity)

    @Query("SELECT * FROM notes WHERE contact_id=:contactId")
    suspend fun findNotesByContactId(contactId : Long) : List<NoteDbEntity>

    // TODO - Should I return Flow here? To observer wether the data is changed
    @Update
    suspend fun updateNote(noteDbEntity : NoteDbEntity)

    @Query("DELETE FROM notes WHERE note_id=:noteId")
    suspend fun deleteNote(noteId : Long)

}