package com.example.contactnotes.model.note.room.entities

data class NoteTuple(
    val note_text : String
)