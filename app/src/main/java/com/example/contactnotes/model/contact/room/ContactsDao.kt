package com.example.contactnotes.model.contact.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.contact.room.entities.ContactDbEntity
import com.example.contactnotes.model.contact.room.entities.ContactTuple

@Dao
interface ContactsDao {

    @Query("SELECT * FROM contacts WHERE id=:id")
    suspend fun findContactById(id : Long) : List<ContactTuple?>

    @Query("SELECT * FROM contacts")
    suspend fun getAllContacts() : List<ContactDbEntity>

    @Insert
    suspend fun addContact(contactDbEntity : ContactDbEntity)

    @Delete
    suspend fun deleteContact(contactDbEntity : ContactDbEntity)

}