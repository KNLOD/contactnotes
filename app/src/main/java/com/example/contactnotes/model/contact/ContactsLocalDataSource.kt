package com.example.contactnotes.model.contact

import android.content.ContentResolver
import android.provider.ContactsContract
import android.util.Log
import com.example.contactnotes.model.contact.entities.Contact

class ContactsLocalDataSource(private val contentResolver : ContentResolver) {

    fun fetchContacts() : List<Contact> {

        val result: MutableList<Contact> = mutableListOf()

        val cursor = contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
            ),
            null,
            null,
            null
        )

        cursor?.let {
            cursor.moveToFirst()
                while (!cursor.isAfterLast()) {
                    Log.d(
                        "CONTACT",
                        "${cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))}, "
                    )
                    result.add(
                        Contact(
                            id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID)),
                            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        )
                    )
                    Log.d("CONTACT", "Added : ${result}")



                    cursor.moveToNext()
                }
                cursor.close()
            }
        return result
    }

}