package com.example.contactnotes

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.contact.room.RoomContactsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [ContactsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactsFragment : Fragment() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val contactViewModel : ContactViewModel by activityViewModels{
            ContactViewModel.ContactViewModelFactory(Repositories.contactsRepository, Repositories.notesRepository)
        }

        val contacts = contactViewModel.contactsDataset.value
        Log.e("Fragment", "$contacts")

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        contactViewModel.contactsDataset.observe(viewLifecycleOwner, Observer{
            Log.e("Fragment", "Dataset Changed")
            recyclerView.adapter = ContactsAdapter(it, contactViewModel)
        })


        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactsFragment()
    }
}