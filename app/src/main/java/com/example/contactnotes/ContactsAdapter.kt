package com.example.contactnotes

import android.content.ContentResolver
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.contactnotes.model.contact.ContactsLocalDataSource
import com.example.contactnotes.model.contact.entities.Contact
import com.example.contactnotes.model.contact.room.ContactsDao
import com.example.contactnotes.model.contact.room.RoomContactsRepository
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ContactsAdapter(private val dataset : List<Contact>,
                      private val contactsViewModel : ContactViewModel
                      ) : RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {


    class ContactsViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val contactName : TextView = view.findViewById(R.id.contact_name)
        val showNotes : FloatingActionButton = view.findViewById(R.id.show_notes)
    }


    override fun onCreateViewHolder(parent : ViewGroup, viewType: Int) : ContactsViewHolder{
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.contact_list_item, parent, false)
        return ContactsViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder : ContactsViewHolder, position : Int){
        val currentContact = dataset[position]
        holder.contactName.text = currentContact.contactName
        holder.showNotes.setOnClickListener{

            contactsViewModel.setCurrentContact(currentContact)
            Navigation.findNavController(it).navigate(R.id.action_contactsFragment_to_contactNotesFragment)
        }


    }
    override fun getItemCount() : Int{
        Log.e("ADAPTER", "$dataset")
        return dataset.size
    }
}