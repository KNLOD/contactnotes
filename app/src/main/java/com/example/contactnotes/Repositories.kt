package com.example.contactnotes

import android.content.Context
import androidx.room.Room
import com.example.contactnotes.model.contact.ContactsLocalDataSource
import com.example.contactnotes.model.contact.room.RoomContactsRepository
import com.example.contactnotes.model.note.room.RoomNotesRepository
import com.example.contactnotes.model.room.AppDatabase


object Repositories{
    private lateinit var applicationContext : Context

    private val database : AppDatabase by lazy<AppDatabase>{
        Room.databaseBuilder(applicationContext, AppDatabase::class.java,"database.db").build()


    }

    val contactsRepository : RoomContactsRepository by lazy{
        RoomContactsRepository(database.getContactsDao(), ContactsLocalDataSource(applicationContext.contentResolver))
    }

    val notesRepository : RoomNotesRepository by lazy{
        RoomNotesRepository(database.getNotesDao())
    }

    fun init(context : Context){
        applicationContext = context
    }

}