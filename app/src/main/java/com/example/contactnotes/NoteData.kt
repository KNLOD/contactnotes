package com.example.contactnotes

data class NoteData(
    val noteId : Long,
    val noteTitle : String,
    val noteText : String,
    val contactId : Long,
)
